import { DetailedCharacterUseCase } from "../src/usecases/detailed-character.usecase";
import { CharactersRepository } from "../src/repositories/characters.repository";
import { CHARACTER } from "./fixtures/detailed-character";

jest.mock("../src/repositories/characters.repository");

describe("Detailed character use case", () => {
  beforeEach(() => {
    CharactersRepository.mockClear();
  });

  it("should get the detailed character", async () => {
    CharactersRepository.mockImplementation(() => {
      return {
        getDetailedCharacter: () => {
          return CHARACTER;
        },
      };
    });

    const id = 1;

    const detailedCharacter = await DetailedCharacterUseCase.execute(id);

    expect(detailedCharacter.name).toBe("Luke Skywalker");
    expect(detailedCharacter.mass).toBe("77");
    expect(detailedCharacter.name).toBe(CHARACTER.result.properties.name);
    expect(detailedCharacter.mass).toBe(CHARACTER.result.properties.mass);
  });
});
