import { AllCharactersUseCase } from "../src/usecases/all-characters.usecase";
import { CharactersRepository } from "../src/repositories/characters.repository";
import { CHARACTERS } from "./fixtures/characters";

jest.mock("../src/repositories/characters.repository");

describe("All characters use case", () => {
  beforeEach(() => {
    CharactersRepository.mockClear();
  });

  it("should get all characters", async () => {
    CharactersRepository.mockImplementation(() => {
      return {
        getAllCharacters: () => {
          return CHARACTERS;
        },
      };
    });

    const characters = await AllCharactersUseCase.execute();

    expect(Array.isArray(characters)).toBe(true);
    expect(characters.length).toBe(10);
    expect(characters[0].name).toBe(CHARACTERS.results[0].name);
    expect(characters[1].id).toBe(CHARACTERS.results[1].uid);
  });
});
