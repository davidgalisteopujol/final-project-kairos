import axios from "axios";

export class CharactersRepository {
  async getAllCharacters() {
    return await (
      await axios.get("https://www.swapi.tech/api/people")
    ).data;
  }

  async getDetailedCharacter(id) {
    return await (
      await axios.get(`https://www.swapi.tech/api/people/${id}`)
    ).data;
  }
}
