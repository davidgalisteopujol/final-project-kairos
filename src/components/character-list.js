import { LitElement, html } from "lit-element";
import { Router } from "@vaadin/router";
import { ModalCharacter } from "./modal-character";

export class CharacterList extends LitElement {
  static get properties() {
    return {
      characters: { type: Array },
      showModal: { type: Boolean },
      selectedCharacter: { type: Number },
    };
  }

  constructor() {
    super();
    this.characters = [];
    this.showModal = false;
    this.selectedCharacter = null;
  }

  handleMoreInformationClick(character) {
    this.selectedCharacter = character.id;
    this.showModal = true;
  }

  handleCloseModal() {
    this.selectedCharacter = null;
    this.showModal = false;
  }

  firstUpdated() {
    const modalCharacter = this.querySelector("modal-character");
    if (modalCharacter) {
      modalCharacter.addEventListener("close", () => this.handleCloseModal());
    } else {
      this.requestUpdate().then(() => {
        const modalCharacter = this.querySelector("modal-character");
        if (modalCharacter) {
          modalCharacter.addEventListener("close", () =>
            this.handleCloseModal()
          );
        }
      });
    }
  }

  render() {
    return html`
      <div class="card-container">
        ${this.characters.map(
          (character) => html`
            <div class="card">
              <img class="card-img"
              src=https://starwars-visualguide.com/assets/img/characters/${character.id}.jpg
              alt="a character of star wars" />
              <h2 class="card-title">${character.name}</h2>
              <div>
                <button
                  id="character"
                  class="card-button"
                  @click="${() => this.handleMoreInformationClick(character)}"
                >
                  More information
                </button>
              </div>
            </div>
          `
        )}
      </div>
      ${this.showModal
        ? html`
            <modal-character
              .selectedCharacter="${this.selectedCharacter}"
              @close-modal="${this.handleCloseModal}"
            ></modal-character>
          `
        : null}
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("character-list", CharacterList);
