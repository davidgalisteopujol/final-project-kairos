import { LitElement, html, css } from "lit-element";
import { DetailedCharacterUseCase } from "../usecases/detailed-character.usecase";
export class ModalCharacter extends LitElement {
  static get properties() {
    return {
      character: { type: Object },
      selectedCharacter: { type: Object },
    };
  }

  static get styles() {
    return css`
      .modal-content {
        position: fixed;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: rgb(44, 43, 43);
        padding: 20px;
        border-radius: 10px;
        max-width: 600px;
        max-height: 600px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.2);
        min-width: 200px;
      }

      .modal-header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        border-bottom: 1px solid #eee;
        padding-bottom: 10px;
        margin-bottom: 10px;
      }

      .modal-title {
        font-size: 24px;
        margin: 0;
        color: yellow;
      }

      .close-button {
        background: none;
        border: none;
        font-size: 24px;
        cursor: pointer;
        color: yellow;
        transition: color 0.2s;
      }

      .close-button:hover {
        color: yellow;
      }

      .attributes-character {
        margin: 10px 0;
        color: yellow;
        align-items: center;
      }
    `;
  }

  constructor() {
    super();
    this.character = null;
  }

  async connectedCallback() {
    super.connectedCallback();
    this.character = await DetailedCharacterUseCase.execute(
      this.selectedCharacter
    );
    this.requestUpdate();
  }

  render() {
    return html`
      <div id="modal-content" class="modal-content">
        <div class="modal-header">
          <h2 class="modal-title">
            ${this.character ? this.character.name : ""}
          </h2>
          <button id="close-button" class="close-button" @click="${this.close}">
            &times;
          </button>
        </div>
        <div class="modal-body">
          <ul>
            <li>
              <p class="attributes-character">
                Height: ${this.character.height}
              </p>
            </li>
            <li>
              <p class="attributes-character">Mass: ${this.character.mass}</p>
            </li>
            <li>
              <p class="attributes-character">
                Hair color: ${this.character.hairColor}
              </p>
            </li>
            <li>
              <p class="attributes-character">
                Skin color: ${this.character.skinColor}
              </p>
            </li>
            <li>
              <p class="attributes-character">
                Gender: ${this.character.gender}
              </p>
            </li>
          </ul>
        </div>
      </div>
    `;
  }

  close() {
    this.dispatchEvent(new Event("close-modal"));
  }
}

customElements.define("modal-character", ModalCharacter);
