import { Character } from "../model/character";
import { CharactersRepository } from "../repositories/characters.repository";

export class AllCharactersUseCase {
  static async execute() {
    const repository = new CharactersRepository();
    const response = await repository.getAllCharacters();
    const characters = response.results;

    return characters.map(
      (character) =>
        new Character({
          id: character.uid,
          name: character.name,
          url: character.url,
        })
    );
  }
}
