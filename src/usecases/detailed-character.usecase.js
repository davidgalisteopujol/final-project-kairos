import { DetailedCharacterModel } from "../model/detailed-character-model";
import { CharactersRepository } from "../repositories/characters.repository";

export class DetailedCharacterUseCase {
  static async execute(id) {
    const repository = new CharactersRepository();
    const response = await repository.getDetailedCharacter(id);
    const character = response.result.properties;
    const detailedCharacter = new DetailedCharacterModel({
      name: character.name,
      height: character.height,
      mass: character.mass,
      hairColor: character.hair_color,
      skinColor: character.skin_color,
      gender: character.gender,
    });
    return detailedCharacter;
  }
}
