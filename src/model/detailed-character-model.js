export class DetailedCharacterModel {
  constructor({ name, height, mass, hairColor, skinColor, gender }) {
    (this.name = name),
      (this.height = height),
      (this.mass = mass),
      (this.hairColor = hairColor),
      (this.skinColor = skinColor),
      (this.gender = gender);
  }
}
