import { MyApp } from "../my-app";
export class CharactersPage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <my-app></my-app>
    `;
  }
}

customElements.define("characters-page", CharactersPage);
