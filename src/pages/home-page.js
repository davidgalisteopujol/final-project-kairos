export class HomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <div class="container-home">
          <h1 class="main-title-home">Welcome to the Star Wars Fan Page</h1>
          <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Star_Wars_Logo..png/640px-Star_Wars_Logo..png" alt="stars wars logo">
        </div>
    `;
  }
}

customElements.define("home-page", HomePage);
