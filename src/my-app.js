import {
  LitElement,
  html,
  css,
} from "../node_modules/lit-element/lit-element.js";
import { CharacterList } from "./components/character-list";
import { AllCharactersUseCase } from "./usecases/all-characters.usecase";

export class MyApp extends LitElement {
  static get properties() {
    return {
      characters: { type: Array },
    };
  }

  constructor() {
    super();
    this.characters = [];
  }

  async connectedCallback() {
    super.connectedCallback();
    this.characters = await AllCharactersUseCase.execute();
  }

  render() {
    return html`
      <character-list .characters="${this.characters}"></character-list>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("my-app", MyApp);
