import "./main.css";
import { Router } from "@vaadin/router";
import { HomePage } from "./pages/home-page";
import { CharactersPage } from "./pages/characters-page";

const main = document.querySelector("#main");
const router = new Router(main);

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/characters", component: "characters-page" },
  { path: "(.*)", redirect: "/" },
]);
